const rides = [
    {
        start: "7:00",
        end: "7:30",
        items: {
            apple: 2,
            brownie: 1
        }
    },
    {
        start: "7:10",
        end: "8:00",
        items: {
            apple: 1,
            carrots: 3
        }
    },
    {
        start: "7:20",
        end: "7:45",
        items: {
            apple: 1,
            brownie: 2,
            diamonds: 4
        }
    }   
];

const basket = {};

function sort(times) {
    return times.sort(function (a, b) {
        return a > b;
    })
}

function statRide(ride) {
    var keys = (Object.keys(ride.items));
    if (keys && keys.length > 0) {
        keys.map(function (key) {
            if (basket[key]) {
                basket[key] = basket[key] + ride.items[key];
            } else {
                basket[key] = ride.items[key];
            }
        });
    }
}

function endRide(ride) {
    var keys = (Object.keys(ride.items));
    if (keys && keys.length > 0) {
        keys.map(function (key) {
            if (basket[key]) {
                basket[key] = basket[key] - ride.items[key];
                if (basket[key] <= 0) {
                    delete basket[key];
                }
            }
        });
    }
}

function porcessRides(rides) {
    var sortedTimes = rides.reduce(function (temp, ride) {
        if (ride.items && Object.keys(ride.items).length) {
            temp.push(ride.start)
            temp.push(ride.end)
        }
        return temp;
    }, []).sort().filter(function (el, i, a) { if (i == a.indexOf(el)) return 1; return 0 });    
    for (let i = 0; i < sortedTimes.length - 1; i++) {        
        rides.forEach(element => {
            if (element.items && Object.keys(element.items).length) {
                if (sortedTimes[i] === element.end) {
                    endRide(element);                    
                } else if (sortedTimes[i] === element.start) {
                    statRide(element);                    
                }
            }
        });
        console.log('------------------------------------');
        console.log(sortedTimes[i] + " - " + sortedTimes[i + 1] + " -> " + JSON.stringify(basket));
        console.log('------------------------------------');
    }
}

porcessRides(rides);